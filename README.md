# api_wrapper_gen

Generates structs and tests for JSON API wrappers, based on a sample of the response.

Taking a file such as this:
```
{
  'int_prop': 15,
  'float_prop': 6.7,
  'bool_prop': true,
  'array_prop': [1,2,3,4],
  'object_prop': {
    'int_prop': 13,
    'float_prop': 15.9,
  },
}
```

Will generate a class file such as this:
```
struct Wrapper
  getter int_prop : Int32
  getter float_prop : Float64
  getter bool_prop : Bool
  getter array_prop : Array(Int32)
  getter object_prop : ObjectProp

  struct ObjectProp
    getter int_prop : Int32
    getter float_prop : Float64
  end
end
```

Optionally, it can also generate a test file:
```
require "minitest/autorun"

require "../../src/wrapper"

class WrapperTest < Minitest::Test
  def test_parses_json
    json = %({"int_prop":15,"float_prop":6.7,"bool_prop":true,"array_prop":[1,2,3,4],"object_prop":{"int_prop":13,"float_prop":15.9}})
    wrapper = Wrapper.from_json(json)

    assert_equal wrapper.int_prop, 15
    assert_equal wrapper.float_prop, 6.7
    assert_equal wrapper.bool_prop, true
    assert_equal wrapper.array_prop, [1,2,3,4]

    object_prop = wrapper.object_prop
    assert_equal object_prop.int_prop, 13
    assert_equal object_prop.float_prop, 15.9
  end
end
```

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     api_wrapper_gen:
       gitlab: HCLarsen/api_wrapper_gen
   ```

2. Run `shards install`

## Usage

```crystal
require "api_wrapper_gen"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/HCLarsen/api_wrapper_gen/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Chris Larsen](https://gitlab.com/HCLarsen) - creator and maintainer
